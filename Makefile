# Makefile for BLFS Book generation.
# By Tushar Teredesai <tushar@linuxfromscratch.org>
# 2004-01-31

# Adjust these to suit your installation
RENDERTMP   ?= $(HOME)/tmp
CHUNK_QUIET  = 1
ROOT_ID      =
HTML_EXT    ?= html
SHELL        = /bin/bash

ALLXML := $(filter-out $(RENDERTMP)/%, \
	$(wildcard *.xml */*.xml */*/*.xml */*/*/*.xml */*/*/*/*.xml))
ALLXSL := $(filter-out $(RENDERTMP)/%, \
	$(wildcard *.xsl */*.xsl */*/*.xsl */*/*/*.xsl */*/*/*/*.xsl))

ifdef V
	Q =
else
	Q = @
endif

ifndef REV
	REV = sysv
endif

ifneq ($(REV), sysv)
	ifneq ($(REV), systemd)
		$(error REV must be 'sysv' (default) or 'systemd'.)
	endif
endif

ifeq ($(REV), sysv)
	BASEDIR          ?= $(HOME)/public_html/blfs-book
	DUMPDIR          ?= $(HOME)/blfs-commands
	BASE_COMBINED    ?= sysv-base-combined.xml
	BASE_PROFILE     ?= sysv-base-profile.xml
	HTML_PROFILE     ?= sysv-html-profile.xml
	PDF_PROFILE      ?= sysv-pdf-profile.xml
	PDF_FO           ?= sysv-pdf.fo
	NOCHUNKS_OUTPUT  ?= blfs-book.$(HTML_EXT)
	PDF_OUTPUT       ?= blfs-book.pdf
else
	BASEDIR          ?= $(HOME)/public_html/blfs-systemd
	DUMPDIR          ?= $(HOME)/blfs-sysd-commands
	BASE_COMBINED    ?= systemd-base-combined.xml
	BASE_PROFILE     ?= systemd-base-profile.xml
	HTML_PROFILE     ?= systemd-html-profile.xml
	PDF_PROFILE      ?= systemd-pdf-profile.xml
	PDF_FO           ?= systemd-pdf.fo
	NOCHUNKS_OUTPUT  ?= blfs-sysd-book.$(HTML_EXT)
	PDF_OUTPUT       ?= blfs-sysd-book.pdf
endif

$(RENDERTMP) $(BASEDIR) $(DUMPDIR):
	$(Q)mkdir -p $@

.PHONY: help world all html nochunks pdf tmpdir clean validate profile-html profile-pdf glfs-patch-list wget-list version test-links bootscripts systemd-units test-options dump-commands

help:
	@echo ""
	@echo "make <parameters> <targets>"
	@echo ""
	@echo "Parameters:"
	@echo "  REV=<rev>            Build variation of book"
	@echo "                       Valid values for REV are:"
	@echo "                       * sysv    - Build book for SysV"
	@echo "                       * systemd - Build book for systemd"
	@echo "                       Defaults to 'sysv'"
	@echo ""
	@echo "  BASEDIR=<dir>        Put the output in directory <dir>."
	@echo "                       Defaults to"
	@echo "                       'HOME/public_html/blfs-book' if REV=sysv (or unset)"
	@echo "                       or to"
	@echo "                       'HOME/public_html/blfs-book-systemd' if REV=systemd"
	@echo ""
	@echo "  V=<val>              If <val> is a non-empty value, all"
	@echo "                       steps to produce the output is shown."
	@echo "                       Default is unset."
	@echo ""
	@echo "Targets:"
	@echo "  help                 Show this help text."
	@echo ""
	@echo "  blfs                 Builds targets 'html' and 'wget-list'."
	@echo ""
	@echo "  html                 Builds the HTML pages of the book."
	@echo ""
	@echo "  pdf                  Builds the book as a PDF file."
	@echo ""
	@echo "  wget-list            Produces a list of all packages to download."
	@echo "                       Output is BASEDIR/wget-list"
	@echo ""
	@echo "  nochunks             Builds the book as a one-pager. The output"
	@echo "                       is a large single HTML page containing the"
	@echo "                       whole book."
	@echo ""
	@echo "                       Parameter NOCHUNKS_OUTPUT=<filename> controls"
	@echo "                       the name of the HTML file."
	@echo ""
	@echo "  validate             Runs validation checks on the XML files."
	@echo ""
	@echo "  test-links           Runs validation checks on URLs in the book."
	@echo "                       Produces a file named BASEDIR/bad_urls containing"
	@echo "                       URLS which are invalid and a BASEDIR/good_urls"
	@echo "                       containing all valid URLs."
	@echo ""

world: all blfs-patch-list dump-commands test-links

all: html nochunks pdf

html: $(BASEDIR)/index.html $(BASEDIR)/wget-list
$(BASEDIR)/index.html: stylesheets/blfs-chunked.xsl $(RENDERTMP)/$(HTML_PROFILE) obfuscate.sh | $(BASEDIR)

#	NOTE: Replace static filenames defined by <?dbhtml?>
#	NOTE: <?dbhtml filename?> directives, and this sed patch, can be removed if
#	      `id`s are defined and chunk option `use.id.as.filename` is set
	$(Q)sed \
		-e '/dbhtml filename/s/.html"/.'"$(HTML_EXT)"'"/' \
		$(RENDERTMP)/$(HTML_PROFILE) \
		> $(RENDERTMP)/$(HTML_PROFILE)-ext

	@echo "Generating chunked XHTML files at $(BASEDIR)/ ..."
	$(Q)xsltproc \
		--nonet \
		--stringparam chunk.quietly $(CHUNK_QUIET) \
		--stringparam rootid "$(ROOT_ID)" \
		--stringparam base.dir $(BASEDIR)/ \
		--stringparam html.ext ".$(HTML_EXT)" \
		stylesheets/blfs-chunked.xsl \
		$(RENDERTMP)/$(HTML_PROFILE)-ext

	@echo "Copying CSS code and images ..."
	$(Q)mkdir -p $(BASEDIR)/stylesheets
	$(Q)cp stylesheets/lfs-xsl/*.css $(BASEDIR)/stylesheets
	$(Q)cp -R images $(BASEDIR)/

	@echo "Running Tidy and obfuscate.sh on chunked XHTML ..."
#	BUG: sed should not have to fix MIME type
	$(Q)for filename in `find $(BASEDIR) -name "*.$(HTML_EXT)"`; \
	do \
		tidy -config tidy.conf $$filename || test $$? -le 1; \
		bash obfuscate.sh $$filename; \
		sed -i \
			-e '1,20s:text/html:application/xhtml+xml:g' \
			$$filename; \
	done

	@echo "Chunked XHTML created at $(BASEDIR)/"

nochunks: $(BASEDIR)/$(NOCHUNKS_OUTPUT) $(BASEDIR)/wget-list
$(BASEDIR)/$(NOCHUNKS_OUTPUT): stylesheets/blfs-nochunks.xsl $(RENDERTMP)/$(HTML_PROFILE) obfuscate.sh | $(BASEDIR)
	@echo "Generating non-chunked XHTML file at $(BASEDIR)/ ..."
	$(Q)xsltproc \
		--nonet \
		--stringparam rootid "$(ROOT_ID)" \
		--output $(BASEDIR)/$(NOCHUNKS_OUTPUT) \
		stylesheets/blfs-nochunks.xsl \
		$(RENDERTMP)/$(HTML_PROFILE)

	@echo "Running Tidy on non-chunked XHTML ..."
	$(Q)tidy -config tidy.conf $(BASEDIR)/$(NOCHUNKS_OUTPUT) || test $$? -le 1

	@echo "Running obfuscate.sh on non-chunked XHTML ..."
	$(Q)bash obfuscate.sh $(BASEDIR)/$(NOCHUNKS_OUTPUT)

#	BUG: sed should not have to fix MIME type
	$(Q)sed -i \
		-e '1,20s:text/html:application/xhtml+xml:g' \
		$(BASEDIR)/$(NOCHUNKS_OUTPUT)

	@echo "$(BASEDIR)/$(NOCHUNKS_OUTPUT) created"

pdf: $(BASEDIR)/$(PDF_OUTPUT) $(BASEDIR)/wget-list
$(BASEDIR)/$(PDF_OUTPUT): stylesheets/blfs-pdf.xsl $(RENDERTMP)/$(PDF_PROFILE) | $(BASEDIR)
	@echo "Generating FO file ..."
	$(Q)xsltproc \
		--nonet \
		--stringparam rootid "$(ROOT_ID)" \
		--output $(RENDERTMP)/$(PDF_FO) \
		stylesheets/blfs-pdf.xsl \
		$(RENDERTMP)/$(PDF_PROFILE)

	$(Q)sed -i \
		-e 's/span="inherit"/span="all"/' \
		$(RENDERTMP)/$(PDF_FO)

#	$(Q)bash pdf-fixups.sh $(RENDERTMP)/$(PDF_FO)

	@echo "Generating PDF file ..."
	$(Q)cp -R images $(RENDERTMP)/

	$(Q)fop -q $(RENDERTMP)/$(PDF_FO) $(BASEDIR)/$(PDF_OUTPUT) 2>fop.log

	@echo "$(BASEDIR)/$(PDF_OUTPUT) created"
	@echo "fop.log created"

clean:
	@echo "Cleaning version files"
	$(Q)rm -f \
		conditional.ent \
		version.ent

	@echo "Cleaning $(BASEDIR)"
	$(Q)rm -rf $(BASEDIR)/*

	@echo "Cleaning $(RENDERTMP)"
	$(Q)rm -rf $(RENDERTMP)/*

	@echo "Cleaning $(DUMPDIR)"
	$(Q)rm -rf $(DUMPDIR)/*

validate: $(RENDERTMP)/$(BASE_PROFILE)
$(RENDERTMP)/$(BASE_PROFILE): stylesheets/lfs-xsl/profile.xsl general.ent packages.ent $(ALLXML) $(ALLXSL) version | $(RENDERTMP)
	@echo "Adjusting for revision $(REV) ..."
	$(Q)xsltproc \
		--nonet \
		--xinclude \
		--stringparam profile.revision $(REV) \
		--output $(RENDERTMP)/$(BASE_COMBINED) \
		stylesheets/lfs-xsl/profile.xsl \
		index.xml

	@echo "Validating the book ..."
	$(Q)xmllint \
		--nonet \
		--encode UTF-8 \
		--postvalid \
		--output $(RENDERTMP)/$(BASE_PROFILE) \
		$(RENDERTMP)/$(BASE_COMBINED)

	@echo "Validation complete."

profile-html: $(RENDERTMP)/$(HTML_PROFILE)
$(RENDERTMP)/$(HTML_PROFILE): stylesheets/lfs-xsl/profile.xsl $(RENDERTMP)/$(BASE_PROFILE)
	@echo "Generating profiled XML for XHTML ..."
	$(Q)xsltproc \
		--nonet \
		--stringparam profile.condition "$(HTML_EXT)" \
		--output $(RENDERTMP)/$(HTML_PROFILE) \
		stylesheets/lfs-xsl/profile.xsl \
		$(RENDERTMP)/$(BASE_PROFILE)

profile-pdf: $(RENDERTMP)/$(PDF_PROFILE)
$(RENDERTMP)/$(PDF_PROFILE): stylesheets/lfs-xsl/profile.xsl $(RENDERTMP)/$(BASE_PROFILE)
	@echo "Generating profiled XML for PDF ..."
	$(Q)xsltproc \
		--nonet \
		--stringparam profile.condition pdf \
		--output $(RENDERTMP)/$(PDF_PROFILE) \
		stylesheets/lfs-xsl/profile.xsl \
		$(RENDERTMP)/$(BASE_PROFILE)

blfs-patch-list: blfs-patches.sh
	@echo "Generating blfs patch list ..."
	$(Q)awk '{if ($$1 == "copy") {sub(/.*\//, "", $$2); print $$2}}' \
		blfs-patches.sh > blfs-patch-list

blfs-patches.sh: stylesheets/patcheslist.xsl $(RENDERTMP)/$(BASE_PROFILE)
	@echo "Generating blfs patch script ..."
	$(Q)xsltproc \
		--nonet \
		--output blfs-patches.sh \
		stylesheets/patcheslist.xsl \
		$(RENDERTMP)/$(BASE_PROFILE)

wget-list: $(BASEDIR)/wget-list
$(BASEDIR)/wget-list: stylesheets/wget-list.xsl $(RENDERTMP)/$(BASE_PROFILE) | $(BASEDIR)
	@echo "Generating wget list for $(REV) at $(BASEDIR)/wget-list ..."
	$(Q)xsltproc \
		--nonet \
		--output $(BASEDIR)/wget-list \
		stylesheets/wget-list.xsl \
		$(RENDERTMP)/$(BASE_PROFILE)

test-links: $(BASEDIR)/test-links
$(BASEDIR)/test-links: $(RENDERTMP)/$(BASE_PROFILE)
	@echo "Generating test-links file ..."
	$(Q)mkdir -p $(BASEDIR)
	$(Q)xsltproc \
		--nonet \
		--stringparam list_mode full \
		--output $(BASEDIR)/test-links \
		stylesheets/wget-list.xsl \
		$(RENDERTMP)/$(BASE_PROFILE)

	@echo "Checking URLs, first pass ..."
	$(Q)rm -f $(BASEDIR)/{good,bad,true_bad}_urls
	$(Q)for URL in `cat $(BASEDIR)/test-links`; \
	do \
		wget --spider --tries=2 --timeout=60 $$URL >>/dev/null 2>&1; \
		if test $$? -ne 0 ; then \
			echo $$URL >> $(BASEDIR)/bad_urls ; \
		else \
			echo $$URL >> $(BASEDIR)/good_urls 2>&1; \
		fi; \
	done

	@echo "Checking URLs, second pass ..."
	$(Q)for URL2 in `cat $(BASEDIR)/bad_urls`; \
	do \
		wget --spider --tries=2 --timeout=60 $$URL2 >>/dev/null 2>&1; \
		if test $$? -ne 0 ; then \
			echo $$URL2 >> $(BASEDIR)/true_bad_urls ; \
		else \
			echo $$URL2 >> $(BASEDIR)/good_urls 2>&1; \
		fi; \
	done

bootscripts:
	@VERSION=`grep "bootscripts-version " general.ent | cut -d\" -f2`; \
	BOOTSCRIPTS="blfs-bootscripts-$$VERSION"; \
	if [ ! -e $$BOOTSCRIPTS.tar.xz ]; then \
		rm -rf $(RENDERTMP)/$$BOOTSCRIPTS; \
		mkdir $(RENDERTMP)/$$BOOTSCRIPTS; \
		cp -a ../bootscripts/* $(RENDERTMP)/$$BOOTSCRIPTS; \
		rm -rf ../bootscripts/archive; \
		tar -cJhf $$BOOTSCRIPTS.tar.xz -C $(RENDERTMP) $$BOOTSCRIPTS; \
	fi

systemd-units:
	@VERSION=`grep "systemd-units-version " general.ent | cut -d\" -f2`; \
	UNITS="blfs-systemd-units-$$VERSION"; \
	if [ ! -e $$UNITS.tar.xz ]; then \
		rm -rf $(RENDERTMP)/$$UNITS; \
		mkdir $(RENDERTMP)/$$UNITS; \
		cp -a ../systemd-units/* $(RENDERTMP)/$$UNITS; \
		tar -cJhf $$UNITS.tar.xz -C $(RENDERTMP) $$UNITS; \
	fi

test-options:
	$(Q)xsltproc --xinclude --nonet stylesheets/test-options.xsl index.xml

version:
	@echo "Generating conditional.ent and version.ent XML entities ..."
	$(Q)./git-version.sh $(REV)

dump-commands: stylesheets/dump-commands.xsl $(RENDERTMP)/$(BASE_PROFILE) | $(DUMPDIR)
	@echo "Dumping book commands at $(DUMPDIR)/ ..."
	$(Q)xsltproc \
		--nonet \
		--output $(DUMPDIR)/ \
		stylesheets/dump-commands.xsl \
		$(RENDERTMP)/$(BASE_PROFILE)

	@echo "Dumping book commands complete at $(DUMPDIR)/"
