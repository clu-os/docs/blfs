#! /bin/sh

# Last tested with container image: docker.io/library/alpine:3:18
# Does not render correctly!

# Install necessary packages
apk update
apk add  \
	musl-locales bash coreutils sed tar \
	xz \
	perl git make docbook-xsl tidyhtml
