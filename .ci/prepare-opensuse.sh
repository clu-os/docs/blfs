#! /bin/sh

# Last tested with container image: registry.opensuse.org/opensuse/leap:15.5

# Install necessary packages
zypper --non-interactive refresh
zypper --non-interactive install --download-in-advance --no-recommends \
	tar \
	xz \
	perl git make libxslt-tools docbook-xsl-stylesheets libxml2-tools tidy
