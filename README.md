
Read the latest [CI/CD](https://en.wikipedia.org/wiki/CI/CD) version online:

- [HTML (separate pages)](https://clu-os.gitlab.io/docs/blfs/)
- [HTML (single page)](https://clu-os.gitlab.io/docs/blfs/blfs-book.xhtml)
- PDF

Differences in this fork:

- [x] [Link relationships](https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes/rel) have been re-enabled (`prev`, `next`, `up`, `home`, `license`)
- [x] [Open Graph](https://ogp.me/) metadata has been added, social timeline cards support (`<meta property="og:*">`)
- [x] Support for configurable filename extension has been re-enabled ([html.ext](https://docbook.sourceforge.net/release/xsl/current/doc/html/html.ext.html))
- [ ] Support for configurable output character encoding ([chunker.output.encoding](https://docbook.sourceforge.net/release/xsl/current/doc/html/chunker.output.encoding.html))
- [x] Some filenames are based on element `id` ([use.id.as.filename](https://docbook.sourceforge.net/release/xsl/current/doc/html/use.id.as.filename.html))
- [ ] Parallel `make`, `WIP`
- [ ] Incremental `make`, `WIP`
- [x] Same `make` targets for all books (`html`, `pdf`, `nochunks`, ...)
- [x] CSS stylesheets have been combined, for all media (`screen` and `print`)
- [ ] "Dark Mode", `TODO`

Visit the official, upstream, [Beyond Linux From Scratch homepage](https://www.linuxfromscratch.org/blfs/)

Original (almost) README below:
___

How do I convert these XML files to HTML? You need to have some software
installed that deal with these conversions. Please read the [`INSTALL.md`](INSTALL.md) file to
determine what programs you need to install and where to get instructions to
install that software.

After that, you can build the HTML with a simple `make` command.
The default target builds the HTML in `$(HOME)/public_html/blfs-book`.

For all targets, setting the parameter `REV=systemd` is needed to build the
`systemd` version of the book.

Other [`Makefile`](Makefile) targets are: `nochunks`, `validate`, and `blfs-patch-list`.

- `nochunks`: builds BLFS in one huge file.

- `validate`:  does an extensive check for XML errors in the book.

- `blfs-patch-list`: generates a list of all BLFS controlled patches in the book.
