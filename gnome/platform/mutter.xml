<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
   "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % general-entities SYSTEM "../../general.ent">
  %general-entities;

  <!ENTITY mutter-download-http "&gnome-download-http;/mutter/&gnome-47;/mutter-&mutter-version;.tar.xz">
  <!ENTITY mutter-download-ftp  " ">
  <!ENTITY mutter-md5sum        "c899a4fed30ce1a99f0e17567b59cfb9">
  <!ENTITY mutter-size          "6.5 MB">
  <!ENTITY mutter-buildsize     "72 MB (with tests)">
  <!ENTITY mutter-time          "0.5 SBU (Using parallelism=4; add 1.4 SBU for tests)">
]>

<sect1 id="mutter" xreflabel="Mutter-&mutter-version;">
  <?dbhtml filename="mutter.html"?>


  <title>Mutter-&mutter-version;</title>

  <indexterm zone="mutter">
    <primary sortas="a-Mutter">Mutter</primary>
  </indexterm>

  <sect2 role="package">
    <title>Introduction to Mutter</title>

    <para>
      <application>Mutter</application> is the window manager for
      <application>GNOME</application>. It is not invoked directly,
      but from <application>GNOME Session</application> (on a
      machine with a hardware accelerated video driver).
    </para>

    &lfs122_checked;

    <bridgehead renderas="sect3">Package Information</bridgehead>
    <itemizedlist spacing="compact">
      <listitem>
        <para>
          Download (HTTP): <ulink url="&mutter-download-http;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Download (FTP): <ulink url="&mutter-download-ftp;"/>
        </para>
      </listitem>
      <listitem>
        <para>
          Download MD5 sum: &mutter-md5sum;
        </para>
      </listitem>
      <listitem>
        <para>
          Download size: &mutter-size;
        </para>
      </listitem>
      <listitem>
        <para>
          Estimated disk space required: &mutter-buildsize;
        </para>
      </listitem>
      <listitem>
        <para>
          Estimated build time: &mutter-time;
        </para>
      </listitem>
    </itemizedlist>

    <bridgehead renderas="sect3">Mutter Dependencies</bridgehead>

    <bridgehead renderas="sect4">Required</bridgehead>
    <para role="required">
      <xref linkend="gnome-settings-daemon"/>,
      <xref linkend="graphene"/>,
      <xref linkend="libei"/>,
      <xref linkend="libxcvt"/>,
      <xref linkend="libxkbcommon"/>, and
      <xref linkend="pipewire"/>
    </para>

    <bridgehead renderas="sect4">Recommended</bridgehead>
    <para role="recommended">
      <xref linkend="desktop-file-utils"/>,
      &gobject-introspection;,
      <xref linkend="libdisplay-info"/>, and
      <xref linkend="startup-notification"/>
    </para>

    <bridgehead renderas="sect4">Recommended (Required to
    build the Wayland compositor)</bridgehead>
    <para role="recommended">
      <xref linkend="libinput"/>,
      <xref linkend="wayland"/>,
      <xref linkend="wayland-protocols"/>, and
      <xref linkend="xwayland"/>
    </para>

    <bridgehead renderas="sect4"
                revision="sysv">Recommended (Runtime)</bridgehead>
    <para role="recommended" revision="sysv">
      <xref linkend="blocaled" role="runtime"/>
    </para>

    <bridgehead renderas="sect4">Optional</bridgehead>
    <para role="optional">
      <!-- sorted as "d" instead of "p" -->
      <xref linkend="python-dbusmock"/> (required for tests),
      <!--<xref linkend="sysprof"/>,-->
      <xref linkend="xorg-server"/> (for X11 sessions),
      <ulink url="&sysprof-url;">sysprof</ulink>,
      <command>Xvfb</command> (from <xref linkend='xorg-server'/> or
      <xref linkend='xwayland'/>),
      <ulink url="&sources-anduin-http;/mutter/xvfb-run">xvfb-run</ulink>,
      and <ulink url='https://gitlab.gnome.org/GNOME/zenity'>zenity</ulink>
      (the X11 tests would only run with all of the last three)
    </para>

    <!-- NOTE: Mutter compiles shipped Cogl and Clutter for it's own
    internal use. This is because upstream have decided that Cogl and
    Clutter should be only used by Mutter in the future, so the development
    of Cogl and Clutter is now performed in the Mutter repository.  The
    "standalone" Cogl and Clutter are considered obsolete now and we have
    archived them.  -->

  </sect2>

  <sect2 role="installation">
    <title>Installation of Mutter</title>

    <para>
      Install <application>Mutter</application> by running the following
      commands:
    </para>

<screen><userinput>mkdir build &amp;&amp;
cd    build &amp;&amp;

meson setup --prefix=/usr       \
            --buildtype=release \
            -D tests=disabled   \
            -D profiler=false   \
            ..                  &amp;&amp;
ninja</userinput></screen>

<!--
    <para>
      To test the results, issue:
      <command>dbus-run-session ninja test</command>. The tests
      require an active X session to run correctly. It is not necessary to
      run a  separate D-bus session if not in a GNOME session, but it
      provides a clean environment in any case. One test,
      <filename>native-headless</filename>, is known to fail.
    </para>
    -->
    <!-- there are no added problems in sysv, AFAICT. Running the tests is
    not easy whatever the init system. But using the instructions below in
    a gnome session on sysv works (no test failure)...
    <para revision='sysv'>
       This package does not come with a test suite that runs properly in the
       SystemV environment.
    </para>
    -->

    <para>
      If you want to run the test suite, ensure
      <xref linkend='python-dbusmock'/> is installed and issue:
    </para>

<screen role='test'><userinput>meson configure -D tests=enabled -D clutter_tests=false &amp;&amp;
ninja test</userinput></screen>

    <para>
      The tests require an active X or wayland session.
      Some tests are flaky (especially under a high system load) so if
      a test fails you can try to re-run it alone with the
      <command>meson test <replaceable>&lt;test name&gt;</replaceable></command>
      command.
      A few tests may fail depending on some system configuration.
      Don't make any mouse or keyboard input while the test suite is
      running or some tests may fail.
      You can also test basic functions of <application>Mutter</application>
      following <xref linkend='mutter-starting' role=','/> after installing
      it.
    </para>

    <para>
      Now, as the <systemitem class="username">root</systemitem> user:
    </para>

<screen role="root"><userinput>ninja install</userinput></screen>

  </sect2>

  <sect2 role="commands">
    <title>Command Explanations</title>

    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
      href="../../xincludes/meson-buildtype-release.xml"/>

    <para>
      <parameter>-D tests=disabled</parameter>: Allow building the package
      without <xref linkend='python-dbusmock'/> installed.  It'll be
      overridden with <command>meson configure</command> if running the
      test suite.
    </para>

    <para>
      <parameter>-D clutter_tests=false</parameter>: Disable
      the tests for the shipped
      <application>Clutter</application> library. The
      <application>Clutter</application> tests are known to
      fail with <parameter>--buildtype=release</parameter>.
    </para>

    <para>
      <parameter>-D profiler=false</parameter>: Allows building this package
      without <application>Sysprof</application>.  Remove this option if
      you've installed <application>Sysprof</application> and want to
      analyze the rendering performance of <application>Mutter</application>.
    </para>

    <para>
      <option>-D libdisplay_info=disabled</option>: This allows building
      this package without <xref linkend='libdisplay-info'/> installed.
    </para>

  </sect2>

  <sect2 role="starting" id="mutter-starting">
    <title>Starting Mutter</title>

    <!-- Mutter as a Wayland compositor works with recent mutter releases.
         IIRC mutter can also be used as a X window manager: just replace
         "twm" with "mutter &dash;&dash;x11" in xinitrc. But I've not tested
         such a configuration recently (I don't build Xorg Server or xinit
         these days.  -->

    <para>
      <application>Mutter</application> is normally used as a component of
      <application>gnome-shell</application>, but it can be used as a
      standalone Wayland compositor too.  To run
      <application>Mutter</application> as a Wayland compositor, in a
      virtual console, issue:
    </para>

    <screen role='nodump'><userinput>mutter --wayland -- vte-2.91</userinput></screen>

    <para>
      Replace <command>vte-2.91</command> with the command line for the
      first application you want in the Wayland session. Note that once this
      application exits, the Wayland session will be terminated.
    </para>

    <para>
      <application>Mutter</application> can also function as a nested
      compositor in another Wayland session.  In a terminal emulator, issue:
    </para>

    <screen role='nodump'><userinput>MUTTER_DEBUG_DUMMY_MODE_SPECS=1920x1080 mutter --wayland --nested -- vte-2.91</userinput></screen>

    <para>
      Replace <replaceable>1920x1080</replaceable> with the size you want
      for the nested Wayland session.
    </para>
  </sect2>

  <sect2 role="content">
    <title>Contents</title>

    <segmentedlist>
      <segtitle>Installed Programs</segtitle>
      <segtitle>Installed Libraries</segtitle>
      <segtitle>Installed Directories</segtitle>

      <seglistitem>
        <seg>
          mutter
        </seg>
        <seg>
          libmutter-15.so and libmutter-test-15.so (optional)
        </seg>
        <seg>
          /usr/{lib,include,libexec/installed-tests,share/{,installed-tests}}/mutter-15
        </seg>
      </seglistitem>
    </segmentedlist>

    <variablelist>
      <bridgehead renderas="sect3">Short Descriptions</bridgehead>
      <?dbfo list-presentation="list"?>
      <?dbhtml list-presentation="table"?>

      <varlistentry id="mutter-prog">
        <term><command>mutter</command></term>
        <listitem>
          <para>
            is a <application>Clutter</application> based compositing
            <application>GTK+</application> Window Manager
          </para>
          <indexterm zone="mutter mutter-prog">
            <primary sortas="b-mutter">mutter</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libmutter">
        <term><filename class="libraryfile">libmutter-15.so</filename></term>
        <listitem>
          <para>
            contains the <application>Mutter</application> API functions
          </para>
          <indexterm zone="mutter libmutter">
            <primary sortas="c-libmutter">libmutter-15.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

      <varlistentry id="libmutter-test">
        <term><filename class="libraryfile">libmutter-test-15.so</filename></term>
        <listitem>
          <para>
            contains the <application>Mutter</application> test suite API
            functions; this library is only installed if the test suite of
            this package is run and it is needed by the
            <application>gnome-shell</application> test suite
          </para>
          <indexterm zone="mutter libmutter-test">
            <primary sortas="c-libmutter-test">libmutter-test-15.so</primary>
          </indexterm>
        </listitem>
      </varlistentry>

    </variablelist>

  </sect2>

</sect1>
